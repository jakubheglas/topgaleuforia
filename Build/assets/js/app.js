$(document).ready(function(){


  var menuHeight = 60; // Should be same as in defaults.scss


  // Header size based on viewport
  var viewportHeight = $(window).height();
  var viewportHeightResize = viewportHeight;
  $(".auto-height").css("height", viewportHeight - menuHeight);
  $(".auto-height .content").css("height", viewportHeight - menuHeight);

  $(window).resize(function() {
    if ($(window).height() > 550) {
      viewportHeightResize = $(window).height();
    } else {
      viewportHeightResize = 550;
    }
    $(".auto-height").css("height", viewportHeightResize - menuHeight)
    $(".auto-height .content").css("height", viewportHeightResize - menuHeight);
  });



  // Hide scroll CTA when scrolled
  $(window.parent.document).scroll(function(){
    var scrollOffset = $(window).scrollTop();

    if (scrollOffset > 140) { $(".scroll-cta").addClass("hide"); };
    if (scrollOffset < 140) { $(".scroll-cta").removeClass("hide"); };
  });



  // Open cart drawer
  $("#openCart").click(function(e) {
    e.preventDefault();
    $("BODY").toggleClass("cart-opened");
    $("BODY").removeClass("menu-opened");
    $("#language").removeClass("hover");
  });

  // Close cart drawer when clicked outside of it
  // TODO: doesnt work, cant target plain #content
  $("BODY.cart-opened #content").click(function(e) {
    e.preventDefault();
    $("BODY").removeClass("cart-opened");
    $("BODY").removeClass("menu-opened");
  });

  // Close cart with a button
  $("#closeCart").click(function(e) {
    e.preventDefault();
    $("BODY").removeClass("cart-opened");
  });



  // Open mobile menu drawer
  $("#openMenu").click(function(e) {
    e.preventDefault();
    $("BODY").removeClass("cart-opened");
    $("BODY").toggleClass("menu-opened");
    $("#language").removeClass("hover");
  });



  // Open language selector on touch screens
  $("#language").click(function() {
    $(this).addClass("hover");
  });

  // Open language selector on desktop
  $("#language").mouseover(function() {
    $(this).addClass("hover");
  });

  // Hide language selector on desktop
  $("#language").mouseout(function() {
    $(this).removeClass("hover");
  });



  // Testimonials switch
  $(".testimonials ARTICLE").click(function(e) {
    e.preventDefault();
    id = $(this).attr('class'); // get id
    id = id.replace(/[^0-9]/g, ''); //get number only
    id = parseInt(id); // make number
    tabCount = $(".tabs > ARTICLE").length;
    if (id >= tabCount) {
      nextId = 1;
    } else {
      nextId = id + 1;
    }
    $(this).removeClass("active");
    $(".tabs-selector A.active").removeClass("active");
    $(".tabs-selector A.item-"+nextId).addClass("active");
    $(".tabs ARTICLE.item-"+nextId).addClass("active");
  });



  // Tab switch
  $(".tabs-selector A").click(function(e) {
    e.preventDefault();
    id = $(this).attr('class'); // get id
    id = id.replace(/[^0-9]/g, ''); //get number only
    id = parseInt(id); // make number
    $(".tabs-selector A.active").removeClass("active");
    $(".tabs-selector A.item-"+id).addClass("active");
    $(".tabs ARTICLE.active").removeClass("active");
    $(".tabs ARTICLE.item-"+id).addClass("active");
  });



  // Click to scroll on CTA
  $(".scroll-cta FIGURE, .scroll-cta SPAN").click(function(e) {
    $('html, body').animate({ scrollTop: viewportHeightResize - menuHeight }, 500)
  });



  // Centering the product aside, making a room for it
  var asideHeight = $("#product ASIDE").height();
  if (viewportHeight >= 620) {
    $("#product ASIDE").css("margin-top", -(asideHeight / 2));
    $("#product ASIDE.in-content").css("top", viewportHeight);
  } else {
    $("#product ASIDE").css("margin-top", 0);
    $("#product ASIDE.in-content").css("top", 620);
  }
  $("#product .tabbed-contents").css("min-height", asideHeight);

  function asideReposition() {
    if (viewportHeightResize >= 620) {
      $("#product ASIDE").css("margin-top", -(asideHeight / 2));
    }
  }

  $(window).resize(function() {
    asideReposition();
  });



  // Move product aside to content area when scrolled
  var wasOnHeader = 1;
  var wasOnContent = 0;

  $(window.parent.document).scroll(function(){
    var scrollOffset = $(window).scrollTop();

    // Fire when on content
    if (scrollOffset > (viewportHeightResize / 2)) {
      if (wasOnHeader == "1") {
        wasOnHeader = 0;
        wasOnContent = 1;

        $("#product ASIDE").addClass("out").delay(200).queue(function(next){
          $(this).addClass("in-content").removeClass("out");
          if (viewportHeightResize >= 620) {
            $(this).css("top", viewportHeightResize);
          } else {
            $(this).css("top", 620);
          }
          asideReposition();
          next();
        });
      }
    }

    // Fire when on header
    if (scrollOffset < (viewportHeightResize / 2)) {
      // Header
      if (wasOnContent == "1") {
        wasOnContent = 0;
        wasOnHeader = 1;

        $("#product ASIDE").addClass("out").delay(200).queue(function(next){
          $(this).removeClass("in-content").css("top", "").removeClass("out");
          asideReposition();
          next();
        });
      }
    };
  });


  // Product type cart switch
  $(".available-colors A").click(function(e) {
    e.preventDefault();
    id = $(this).attr('class'); // get id
    id = id.replace(/[^0-9]/g, ''); //get number only
    id = parseInt(id); // make number
    $(".available-colors A.selected").removeClass("selected");
    $(this).addClass("selected");
    $(".available-colors INPUT").val(id);
  });


  // Products grid - Item height
  function resizeProductsHeight() {
    $("#products .product-list ARTICLE").each(function(){
      itemHeight = $(this).outerHeight();
      $(this).children("A.overlay").css('line-height', itemHeight+"px");
    });
  }

  resizeProductsHeight(); // Resize on load
  $(window).resize(function() { // Resize on resize :)
    resizeProductsHeight();
  });


  // FORMS
  // Dropdown
  $('SPAN.select SELECT').on('change', function() {
    var value = $(this).children("option:selected").text();
    $(this).siblings(".value").text(value);
  });
});



// HighSlide lightbox configuration
hs.graphicsDir = 'assets/images/highslide/';
hs.align = 'center';
hs.transitions = ['expand', 'crossfade'];
hs.fadeInOut = true;
hs.dimmingOpacity = 0.8;
hs.marginBottom = 60; // make room for the thumbstrip and the controls
hs.showCredits = false;
hs.zIndexCounter = 99999;

// Add the slideshow providing the controlbar and the thumbstrip
hs.addSlideshow({
  //slideshowGroup: 'group1',
  interval: 5000,
  repeat: false,
  useControls: false,
  overlayOptions: {
    className: 'text-controls',
    position: 'bottom center',
    relativeTo: 'viewport',
    offsetY: -60
  },
  thumbstrip: {
    position: 'bottom center',
    mode: 'horizontal',
    relativeTo: 'viewport'
  }
});
